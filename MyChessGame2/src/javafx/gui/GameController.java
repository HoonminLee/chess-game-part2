package javafx.gui;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;

/**
 *
 * @author Hoon Min Lee
 */
public class GameController {
	private int row = 1;
	private int col = 7;
	private int row2 = 0;
	private int col2 = 0;
	public GridPane p = new GridPane();
	public FlowPane p1 = new FlowPane();
	Image image = new Image(getClass().getResourceAsStream("king.png"));
	Image image2 = new Image(getClass().getResourceAsStream(""));
	Label[][] r = new Label[8][8];
	Button b = new Button("start Game");
	Button b1 = new Button("save");
	Button b2 = new Button("restart");
	TextField messages = new TextField();

	/* a constructor used to create the user interface */
	public GameController() {

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				r[i][j] = new Label(" ");
				r[i][j].setStyle("-fx-border-color: black");
				r[i][j].setPrefSize(90, 90);
				if ((i + j) % 2 == 1) {
					r[i][j].setStyle("-fx-background-color: black");
				} else {
					r[i][j].setStyle("-fx-background-color: white");
				}
				p.add(r[i][j], i, j);
				p.setGridLinesVisible(true);
			}
		}
		// loading an image to location row 1 column 7 once the startgame button
		// is clicked
		b.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				r[row][col].setGraphic(new ImageView(image));
				//r[row2][col2].setGraphic(new ImageView(image));
				messages.setPrefWidth(500);
				messages.setText("game started. Click any location to move the image");
			}
		});
		
		// adding the two buttons and textfield to the flowpane
		p1.getChildren().addAll(b, b1, messages, b2);

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				int x, y;
				x = i;
				y = j;
				r[i][j].setOnMouseClicked(new EventHandler<MouseEvent>() {

					@Override
					public void handle(MouseEvent e) {

						processClick(x, y);
					}
				});
				
			}
		}
		
	}
	

	/*
	 * the method implementation that checks the validity of the click in the
	 * process of moving the object
	 */
	private boolean isValidMove(int i, int j) {
		int rd = (Math.abs(i - row));
		int cd = Math.abs(j - col);

		if ((rd == 1) && (cd == 2)) {
			return true;
		}
		if ((rd == 2) && (cd == 1)) {
			return true;
		}
		return false;
	}

	private void processClick(int i, int j) {

		if (isValidMove(i, j) == false) {
			messages.setText("wrong move. try again");
			return;
		}
		r[row][col].setGraphic(new ImageView(image2));

		r[i][j].setGraphic(new ImageView(image));
		messages.setText("good move. move again");

		row = i;
		col = j;
		b1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
					int currentrow =p.getColumnIndex(r[row][col]);
					int currentcol=p.getRowIndex(r[row][col]);
					messages.setText("the current row is " +currentrow+"and current column is "+currentcol + " The values are saved in loadings text file");
										 
				    try {
				    	 BufferedWriter bf = new BufferedWriter(new FileWriter("loadings.txt"));
				    	 bf.write(currentcol);
				    	 bf.newLine();
				    	 bf.write(currentrow);
				    } catch (IOException e) {
				      throw new RuntimeException(e);
				     				    }
				    b2.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							r[1][7].setGraphic(new ImageView(image));
							r[currentrow][currentcol].setGraphic(new ImageView(image2));
							r[row2][col2].setGraphic(new ImageView(image));
							messages.setPrefWidth(500);
							messages.setText("game restarted. Click any location to move the image");
						}
					});
				    
				   
				  }
			
		});
		
	}
}
