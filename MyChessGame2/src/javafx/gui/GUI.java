/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx.gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.gui.GameController;

/**
 *
 * @author Hon Min Lee. An implementation of the the previous game in javafx
 */
public class GUI extends Application {

	@Override
	/*
	 * The program GUI class uses GameEngine class to display the Graphical User
	 * Interface developed
	 */
	public void start(Stage primaryStage) throws Exception {
		GameController g = new GameController();// creating an object of class
												// GameEngine
		// using a border pane add the flowpane and gridpane int the scene
		BorderPane pane = new BorderPane();
		pane.setCenter(g.p);
		pane.setTop(g.p1);
		Scene scene = new Scene(pane, 500, 500);
		primaryStage.setTitle("chess board");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}

}
